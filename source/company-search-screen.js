import React from 'react';
import {StyleSheet, ScrollView, View, Text} from 'react-native';
import CompanySearch from './search/components/company-search';
import RecentlyViewed from './search/components/recently-viewed';
import Most from './search/components/most';
import CompanyIndustries from './search/components/company-industries';
import Markets from './search/components/markets';
import globalStyles from '../assets/stylesheet/global-stylesheet';

let color = globalStyles.color;

function CompanySearchScreen() {
  return (
    <View style={style.container}>
      <CompanySearch />
      <ScrollView showsVerticalScrollIndicator={false}>
        <RecentlyViewed />
        <Most />
        <CompanyIndustries />
        <Markets />
      </ScrollView>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: color.blue,
  },
  scroll_view: {
    marginTop: 10,
  },
});

export default CompanySearchScreen;
