import React, {useEffect, useState} from 'react';
import {
  VictoryBar,
  VictoryChart,
  VictoryLabel,
  VictoryTheme,
} from 'victory-native';
import HeaderH1 from '../../components/header-h1';
import axios from 'axios';
import Site from '../../helpers/site-helper';

function Chart52Week(props) {
  const [week52s, setWeek52s] = useState([
    {x: 'Low', y: 0},
    {x: 'High', y: 0},
  ]);

  useEffect(() => {
    const fetchCompany52s = async () => {
      const result = await axios(
        `${Site.getURL()}/company?symbol=${props.symbol}`,
      );

      const {fifty_low, fifty_high} = result.data;
      if (fifty_low !== undefined) {
        setWeek52s([
          {x: 'Low', y: fifty_low},
          {x: 'High', y: fifty_high},
        ]);
      }
    };

    fetchCompany52s().done();
  }, []);

  return (
    <>
      <HeaderH1 h1="52 Week High" />
      <VictoryChart theme={VictoryTheme.material} domainPadding={100}>
        <VictoryBar
          data={week52s}
          barWidth={100}
          labels={({datum}) => datum.y}
          style={{data: {fill: '#e14eca'}, labels: {fill: 'white'}}}
          labelComponent={<VictoryLabel dy={30} />}
        />
      </VictoryChart>
    </>
  );
}

export default Chart52Week;
