import React, {useState, useEffect} from 'react';
import {Text} from 'react-native';
import axios from 'axios';
import Site from '../../helpers/site-helper';

function SimilarTicker(props) {
  useEffect(() => {
    fetchLastDaysTicker().done();
  }, []);

  const [previousDay, setPreviousDay] = useState({
    open: 0,
    close: 0,
    change: '0',
  });

  function calculatePreviousChange(open, close) {
    let difference = open - close;
    let change = ((difference / open) * 100).toFixed(2);
    if (change < 0) {
      return `%${Math.abs(change)} (${difference.toFixed(2)})`;
    } else {
      return `%${change} (${difference.toFixed(2)})`;
    }
  }

  const fetchLastDaysTicker = async () => {
    try {
      const result = await axios(
        `${Site.getURL()}/ticker_previous_day?symbol=${props.symbol}`,
      );
      const {open, close} = result.data.values;

      let open_float = parseFloat(open);
      let close_float = parseFloat(close);

      setPreviousDay({
        open: open_float,
        close: close_float,
        change: calculatePreviousChange(open_float, close_float),
      });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Text
      style={{
        marginTop: 10,
        fontSize: 14,
        color: previousDay.change.includes('-') ? 'red' : 'green',
      }}>
      {previousDay.change}
    </Text>
  );
}

export default SimilarTicker;
