import React from 'react';
import {VictoryChart, VictoryPie, VictoryTheme} from 'victory-native';
import HeaderH1 from '../../components/header-h1';

function RecommendedChart() {
  return (
    <>
      <HeaderH1 h1="Recommended" />
      <VictoryChart
        theme={VictoryTheme.material}
        style={{
          data: {fill: 'tomato', opacity: 0.7},
          labels: {fontSize: 12},
          parent: {border: '1px solid #ccc'},
        }}>
        <VictoryPie
          colorScale={['#3C62A4', '#B847BB', '#599C99', '#DE5E90']}
          data={[
            {x: 'buy', y: 50},
            {x: 'hold', y: 10},
            {x: 'sell', y: 40},
            {x: 'strong buy', y: 55},
          ]}
          style={{
            labels: {fill: 'white', fontSize: 14, fontWeight: 'bold'},
          }}
          events={[
            {
              target: 'data',
              eventHandlers: {
                onPressIn: () => {
                  return [
                    {
                      target: 'data',
                      mutation: dataProps => {
                        alert(`item selected is ${dataProps.index}`);
                        return {};
                      },
                    },
                  ];
                },
                onPressOut: () => {},
              },
            },
          ]}
        />
      </VictoryChart>
    </>
  );
}

export default RecommendedChart;
