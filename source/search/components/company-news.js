import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, Linking} from 'react-native';
import axios from 'axios';
import HeaderH1 from '../../components/header-h1';
import shortid from 'shortid';
import Site from '../../helpers/site-helper';
import NewsHelper from '../../helpers/news-helper';
import OS from '../../helpers/os';

function CompanyNews(props) {
  const [newsData, setNews] = useState([]);

  useEffect(() => {
    const fetchNewsData = async () => {
      try {
        const result = await axios(
          `${Site.getURL()}/news?symbol=${props.symbol}&limit=3`,
        );
        setNews(result.data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchNewsData().done();
  }, []);

  function getNews() {
    let n = newsData.map(news => {
      return (
        <TouchableOpacity
          id={shortid.generate()}
          onPress={() => {
            Linking.openURL(news.article.url);
          }}
          style={{marginBottom: 15}}>
          <View id={shortid.generate()} style={{flexDirection: 'row'}}>
            <Image
              style={{height: 100, width: 120}}
              source={{
                uri: news.article.url_image,
              }}
            />

            <View
              id={shortid.generate()}
              style={{flex: 1, paddingLeft: 5, paddingTop: OS.ios() ? 5 : 0}}>
              <Text style={{color: 'white', flexWrap: 'wrap', fontSize: 16}}>
                {NewsHelper.removePublisher(
                  news.title,
                  news.article.source.name,
                )}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View
                  id={shortid.generate()}
                  style={{flex: 0.5, alignItems: 'flex-start'}}>
                  <Text
                    style={{
                      marginTop: OS.ios() ? 20 : 15,
                      color: 'white',
                      fontSize: 18,
                    }}>
                    {NewsHelper.getDate(news.published_at.$date)}
                  </Text>
                </View>
                <View
                  id={shortid.generate()}
                  style={{flex: 0.5, alignItems: 'flex-end'}}>
                  <Image
                    style={{
                      marginTop: OS.ios() ? 14 : 10,
                      marginRight: 10,
                      height: 30,
                      width: 30,
                    }}
                    source={{
                      uri: NewsHelper.getSourceImage(news.article.source.id),
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });

    return <View id={shortid.generate()}>{n}</View>;
  }

  return (
    <View style={{padding: 10}}>
      <HeaderH1 h1="Related news" />
      <View style={{marginTop: 10}}>{getNews()}</View>
    </View>
  );
}

export default CompanyNews;
