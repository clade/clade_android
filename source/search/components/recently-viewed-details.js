import React, {useEffect, useState} from 'react';
import {Image, Text, View} from 'react-native';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import LogoHelper from '../../helpers/logo-helper';
import {useIsFocused} from '@react-navigation/native';

let color = globalStyles.color;

function RecentlyViewedDetails(props) {
  const [companyDetails, setCompanyDetails] = useState('');
  const [previousDay, setPreviousDay] = useState({
    open: 0,
    close: 0,
    change: '',
  });
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      recentlyViewed().done();
      fetchLastDaysTicker().done();
    }
  }, [isFocused]);

  function calculatePreviousChange(open, close) {
    let difference = open - close;
    let change = ((difference / open) * 100).toFixed(2);
    if (change < 0) {
      return `%${Math.abs(change)} (${difference.toFixed(2)})`;
    } else {
      return `%${change} (${difference.toFixed(2)})`;
    }
  }

  const fetchLastDaysTicker = async () => {
    try {
      const result = await axios(
        `${Site.getURL()}/ticker_previous_day?symbol=${props.symbol}`,
      );
      const {open, close} = result.data.values;

      let open_float = parseFloat(open);
      let close_float = parseFloat(close);
      setPreviousDay({
        open: open_float,
        close: close_float,
        change: calculatePreviousChange(open_float, close_float),
      });
    } catch {}
  };

  const recentlyViewed = async () => {
    const result = await axios(
      `${Site.getURL()}/company_details?symbol=${props.symbol}`,
    );
    setCompanyDetails(result.data);
  };

  return (
    <View style={{padding: 8}}>
      <View style={{flexDirection: 'row'}}>
        <Text style={{flex: 2, fontSize: 16, color: 'white'}}>
          {props.symbol}
        </Text>
        <Image
          style={{borderRadius: 5, height: 30, width: 30}}
          source={{
            uri: LogoHelper.get(companyDetails.url),
          }}
        />
      </View>
      <View>
        <Text style={{marginTop: 5, fontSize: 12, color: color.light_gray}}>
          {companyDetails.industry}
        </Text>
        <Text
          style={{
            marginTop: 10,
            fontSize: 14,
            color: previousDay.change.includes('-')
              ? color.stock_red
              : color.stock_green,
          }}>
          {previousDay.change}
        </Text>
      </View>
    </View>
  );
}

export default RecentlyViewedDetails;
