import Autocomplete from 'react-native-autocomplete-input';
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import LogoHelper from '../../helpers/logo-helper';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import {useNavigation} from '@react-navigation/native';
import OS from '../../helpers/os';

let color = globalStyles.color;

function CompanyNewsSearch() {
  const [companies, setCompanies] = useState([]);
  const [query, setQuery] = useState('');
  const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
  const navigation = useNavigation();

  useEffect(() => {
    const fetchMarkets = async () => {
      const result = await axios(`${Site.getURL()}/all_companies`);
      setCompanies(result.data);
    };

    fetchMarkets().done();
  }, []);

  function findCompany(query) {
    if (query === '') {
      return [];
    }

    const regex = new RegExp(`${query.trim()}`, 'i');
    try {
      return companies.filter(str => {
        return regex.test(str.symbol) || regex.test(str.name);
      });
    } catch (e) {
      return [];
    }
  }

  function getData() {
    return findCompany(query).length === 1 &&
      comp(query, findCompany(query)[0].name)
      ? []
      : findCompany(query);
  }

  return (
    <View style={styles.autocompleteContainer}>
      <Autocomplete
        autoCapitalize="none"
        autoCorrect={false}
        data={getData()}
        defaultValue={query}
        onChangeText={(text) => setQuery(text)}
        placeholder="Company news search"
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('CompanyNewsScreen', {symbol: item.symbol})
            }
            style={{
              marginTop: 6,
              marginBottom: 5,
              flexDirection: 'row',
              height: 45,
              borderWidth: 1,
              borderColor: color.blue,
              borderBottomColor: color.pink,
            }}>
            <View style={{flex: 0.1}}>
              <Image
                style={{marginLeft: 5, borderRadius: 5, height: 30, width: 30}}
                source={{
                  uri: LogoHelper.get(item.url),
                }}
              />
            </View>
            <View style={{flex: 0.9}}>
              <Text
                style={{
                  paddingLeft: OS.ios() ? 5 : 10,
                  marginTop: OS.ios() ? 5 : 3,
                  color: 'white',
                  fontSize: 16,
                }}>
                {item.name} ({item.symbol})
              </Text>
            </View>
            <View style={{flex: 0.1}}>
              <Text
                style={{
                  textAlign: 'right',
                  marginTop: 10,
                  color: color.stock_green,
                  fontSize: 16,
                }}>
                {item.percent_change}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item.name.toString()}
        listStyle={{backgroundColor: color.blue}}
        containerStyle={{borderColor: 'green'}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    flex: 1,
    left: 10,
    position: 'absolute',
    right: 10,
    top: 0,
    zIndex: 1,
  },
});

export default CompanyNewsSearch;
