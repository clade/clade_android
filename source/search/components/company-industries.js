import React, {useState, useEffect} from 'react';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import {FlatList, Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import IndustryHelper from '../../helpers/industry-helper';
import {useNavigation} from '@react-navigation/native';

function CompanyIndustries() {
  const navigation = useNavigation();
  const [companyIndustryDetails, setCompanyIndustryDetails] = useState([]);

  useEffect(() => {
    const fetchIndustries = async () => {
      const result = await axios(`${Site.getURL()}/company_details_industry`);

      const [first] = result.data;

      let industry = [];
      for (const [key, value] of Object.entries(first)) {
        industry.push({industry: key, count: value});
      }

      setCompanyIndustryDetails(industry);
    };

    fetchIndustries().done();
  }, []);

  const renderIndustry = ({item}) => {
    let industry = IndustryHelper.get(item.industry);

    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('CompanySearchByIndustry', {
            industry_item: item,
            industry_data: industry,
          })
        }
        style={{
          borderRadius: 5,
          padding: 5,
          backgroundColor: industry.color,
          marginRight: 15,
          height: 140,
          width: 110,
        }}>
        <Icon name={industry.icon} size={28} color="white" />
        <Text
          style={{
            marginTop: 5,
            color: 'white',
            fontSize: 14,
            fontWeight: 'bold',
          }}>
          {item.industry}
        </Text>
        <Text
          style={{
            marginTop: 5,
            color: 'white',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          {item.count}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{marginTop: 5}}>
      <View
        style={{
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}>
        <Text style={{color: 'white', fontSize: 24}}>By Industry</Text>
      </View>

      <View style={{marginTop: 20, marginBottom: 10}}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          legacyImplementation={false}
          data={companyIndustryDetails}
          renderItem={item => renderIndustry(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
}

export default CompanyIndustries;
