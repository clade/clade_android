import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Image, FlatList, Alert} from 'react-native';
import config from '../config';
import axios from 'axios';
import Site from './helpers/site-helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LogoHelper from './helpers/logo-helper';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import globalStyles from '../assets/stylesheet/global-stylesheet';
import AsyncStorage from '@react-native-async-storage/async-storage';

let color = globalStyles.color;

function DashBoardScreen() {
  const navigation = useNavigation();
  const [companyPortfolioData, setCompanyPortfolioData] = useState([]);
  const [totalValue, setTotalValue] = useState(0);
  const [totalGainLoss, setGainLoss] = useState(0);
  const [deleteConfirm, setDeleteConfirm] = useState(true);
  const [priceSelected, setPriceSelected] = useState(true);
  const [viewType, setViewType] = useState('price');
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      getPortfolioPrice().done();
      settings().done();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  useEffect(() => {
    getPortfolioPrice().done();
    settings().done();
  }, []);

  const settings = async () => {
    const settings_key = await AsyncStorage.getItem(config.settings_key);
    let settings_js = JSON.parse(settings_key);
    const {portfolio_delete_confirmation} = settings_js;
    setDeleteConfirm(portfolio_delete_confirmation);
  };

  const calculateTotals = (portfolio_company_data) => {
    if (portfolio_company_data.length === 0) {
      setTotalValue(0);
      setGainLoss(0);
    }

    let currentTotals = portfolio_company_data.map((cp) => {
      const {no_shares} = cp.stock;
      return no_shares * parseFloat(cp.values.close);
    });

    let currentTotalValue = currentTotals.reduce((a, b) => a + b, 0);
    setTotalValue(currentTotalValue.toFixed(2));

    let portfolioTotals = portfolio_company_data.map((cp) => {
      const {no_shares, price_per_share} = cp.stock;
      return no_shares * price_per_share;
    });

    let portfolioTotalValue = portfolioTotals.reduce((a, b) => a + b, 0);

    setGainLoss((currentTotalValue - portfolioTotalValue).toFixed(2));
  };

  const getPortfolioPrice = async () => {
    const portfolio_key = await AsyncStorage.getItem(config.portfolio_key);
    let portfolio = JSON.parse(portfolio_key);
    if (portfolio.length > 0) {
      let symbols = portfolio.map((stocks) => stocks.symbol);

      const result = await axios(
        `${Site.getURL()}/companies_wish_list?symbols=${symbols}`,
      );

      const {companies, ticker} = result.data;
      //todo: This should be handled by the API
      console.log(companies, ticker);
      let merged_by_symbols = companies.map((company) =>
        Object.assign(
          company,
          ticker.find((tick) => tick.symbol === company.symbol),
        ),
      );

      let portfolio_company_data = portfolio.map((stock) => {
        let found = merged_by_symbols.find(
          (company) => company.symbol === stock.symbol,
        );
        return Object.assign(stock, found);
      });

      calculateTotals(portfolio_company_data);
      setCompanyPortfolioData(portfolio_company_data);
    } else {
      setCompanyPortfolioData([]);
      calculateTotals([]);
      //HERE!
    }
  };

  async function delFromPortfolio(symbol) {
    if (deleteConfirm) {
      Alert.alert(
        `${symbol}`,
        `Do you want to remove ${symbol} from your Portfolio`,
        [
          {
            text: `Yes remove ${symbol}`,
            onPress: () => proceedToDelete(symbol).done(),
          },
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: true},
      );
    } else {
      proceedToDelete(symbol).done();
    }
  }

  const proceedToDelete = async (symbol) => {
    const portfolio_key = await AsyncStorage.getItem(config.portfolio_key);
    if (portfolio_key !== null) {
      let portfolio = JSON.parse(portfolio_key);

      const index = portfolio.findIndex((stock) => stock.symbol === symbol);
      if (index > -1) {
        portfolio.splice(index, 1);

        await AsyncStorage.setItem(
          config.portfolio_key,
          JSON.stringify(portfolio),
        );
        getPortfolioPrice().done();
      }
    }
  };

  function calculatePreviousChange(item) {
    if (viewType === 'details') {
      return item.stock.price_per_share;
    }
    let value = item.stock.no_shares * item.stock.price_per_share;
    return value.toFixed(2);
  }

  const calculateStock = (item) => {
    if (viewType === 'details') {
      return `${item.stock.no_shares}`;
    }
    let value = item.stock.no_shares * parseFloat(item.values.close);
    return value.toFixed(2);
  };

  const calculateGainLoss = (item) => {
    if (viewType === 'details') {
      return `$${calculatePreviousChange(item)}`;
    }
    return (calculateStock(item) - calculatePreviousChange(item)).toFixed(2);
  };

  const _styleCalculateGainLoss = (item) => {
    if (viewType === 'details') {
      return 'white';
    }

    if (calculateGainLoss(item) >= 0) {
      return 'green';
    }
    return 'red';
  };

  const renderItem = ({item}) => (
    <View style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
      <View style={{flex: 0.9, height: 50}}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('CompanyDetailsScreen', {symbol: item.symbol})
          }
          style={{flexDirection: 'row'}}>
          <View
            style={{
              marginRight: 5,
              justifyContent: 'center',
              alignItems: 'center',
              flex: 0.15,
            }}>
            <Image
              style={{borderRadius: 5, height: 35, width: 35}}
              source={{
                uri: LogoHelper.get(item.url),
              }}
            />
          </View>

          <View style={{flex: 0.7}}>
            <Text style={{color: 'white', fontSize: 17, marginBottom: 1}}>
              {item.name}
            </Text>
            <Text style={{color: 'gray'}}>{item.symbol}</Text>
          </View>

          <View style={{flex: 0.25, alignItems: 'flex-end', marginTop: 1}}>
            <Text style={{color: 'white', fontSize: 14}}>
              {calculateStock(item)}
            </Text>
            <Text style={{color: _styleCalculateGainLoss(item)}}>
              {calculateGainLoss(item)}
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={{flex: 0.1, alignItems: 'flex-end', marginTop: 5}}>
        <TouchableOpacity onPress={() => delFromPortfolio(item.symbol)}>
          <Icon name="trash" size={22} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );

  const switchView = (type) => {
    setViewType(type);
    getPortfolioPrice().done();
    setPriceSelected(!priceSelected);
  };

  return (
    <View style={style.container}>
      <View
        style={{
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}>
        <Text style={{color: 'white', fontSize: 24}}>Portfolio value</Text>
      </View>

      <View style={{flexDirection: 'row', marginTop: 15}}>
        <View style={{flex: 2}}>
          <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
            ${totalValue}
          </Text>
        </View>
        <Text
          style={{color: totalGainLoss >= 0 ? 'green' : 'red', fontSize: 20}}>
          {totalGainLoss}
        </Text>
      </View>

      <View style={{flexDirection: 'row', marginTop: 10}}>
        <View style={{flex: 2}}>
          <Text style={{color: 'gray', fontSize: 13}}>Total value</Text>
        </View>
        <Text style={{color: 'gray', fontSize: 13}}>Total gain/loss</Text>
      </View>

      <View
        style={{
          marginTop: 15,
          marginBottom: 20,
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}
      />

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
          marginRight: 30,
        }}>
        <View
          style={{
            borderBottomWidth: 3,
            borderBottomColor: priceSelected ? color.pink : color.blue,
          }}>
          <Text
            onPress={() => switchView('price')}
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'white',
              padding: 10,
            }}>
            Price
          </Text>
        </View>
        <View
          style={{
            borderBottomWidth: 3,
            borderBottomColor: !priceSelected ? color.pink : color.blue,
          }}>
          <Text
            onPress={() => switchView('details')}
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'white',
              padding: 10,
            }}>
            Details
          </Text>
        </View>
      </View>

      <FlatList
        data={companyPortfolioData}
        renderItem={renderItem}
        keyExtractor={(item) => item.symbol}
      />
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1a1e34',
    padding: 15,
  },
});

export default DashBoardScreen;
