import {Platform} from 'react-native';

class OS {
  static ios() {
    return Platform.OS === 'ios';
  }
}

export default OS;
