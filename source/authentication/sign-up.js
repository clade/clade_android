import React, {useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import SignNavComponent from './components/sign-nav';
import Amplify, {Auth} from 'aws-amplify';
import awsconfig from '../../aws-exports';
import CladeAlert from '../components/clade-alert';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import ImageHelper from '../helpers/image-helper';
import {useNavigation} from '@react-navigation/native';
import OS from '../helpers/os';
import PasswordInputText from 'react-native-hide-show-password-input';

Amplify.configure(awsconfig);

let color = globalStyles.color;

function SignUpScreen() {
  const navigation = useNavigation();

  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [signUpSpinner, setSignUpSpinner] = useState(false);
  const [signUpAlert, setSignUpAlert] = useState({status: false, message: ''});

  const validateEmail = (email_value) => {
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return expression.test(String(email_value).toLowerCase());
  };

  const validatePassword = (pwd) => {
    const expression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/;

    return expression.test(String(pwd));
  };

  const checkUserInput = () => {
    if (!userName.trim()) {
      setSignUpAlert({status: true, message: 'Please enter user name'});
      return;
    }

    if (!validateEmail(email.trim())) {
      setSignUpAlert({status: true, message: 'Please enter a valid email'});
      return;
    }

    if (
      !password.trim().length === 0 ||
      validatePassword(password.trim()) === false
    ) {
      setSignUpAlert({
        status: true,
        message:
          'Please enter a minimum 8 digit password with numbers, special chars, uppercase and lowercase values',
      });
      return;
    }

    if (password.trim() !== repeatPassword.trim()) {
      setSignUpAlert({status: true, message: 'Password does not match'});
      return;
    }

    signUp().done();
  };

  async function signUp() {
    setSignUpSpinner(true);
    try {
      await Auth.signUp({
        username: userName,
        password: password,
        attributes: {
          email: email,
        },
      })
        .then((res) => {
          console.log('Signed Up', res);
          setSignUpSpinner(false);
          navigation.navigate('VerifySignUpScreen', {
            username: userName,
          });
        })
        .catch((err) => {
          console.log('Failed', err);
          const {message} = err;
          setSignUpSpinner(false);
          setSignUpAlert({status: true, message: message});
        });
    } catch (error) {
      setSignUpSpinner(false);
      const {message} = error;
      setSignUpAlert({status: true, message: message});
      console.log('Error signing in...', error);
    }
  }

  function SignUpObject() {
    return (
      <>
        <Image style={style.clade_image} source={ImageHelper.cladeSmall()} />
        <Text style={style.welcome}>Welcome,</Text>
        <Text style={style.sign_up_to}>sign up to continue</Text>
        <TextInput
          style={style.text_input}
          onChangeText={(text) => setUserName(text)}
          placeholder="User Name"
          value={userName}
          autoCompleteType="off"
          username
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          placeholderTextColor="white"
        />
        <TextInput
          style={style.text_input}
          onChangeText={(text) => setEmail(text)}
          placeholder="Email"
          value={email}
          autoCompleteType="off"
          username
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          placeholderTextColor="white"
        />
        <PasswordInputText
          style={style.password_text_input}
          onChangeText={(text) => setPassword(text)}
          placeholder="Password"
          defaultValue={password}
          placeholderTextColor="white"
          iconColor="white"
          label=""
          activeLineWidth={0}
          disabledLineWidth={0}
          lineWidth={0}
          textColor="white"
        />
        <PasswordInputText
          style={style.password_text_input}
          onChangeText={(text) => setRepeatPassword(text)}
          placeholder="Repeat Password"
          defaultValue={repeatPassword}
          placeholderTextColor="white"
          iconColor="white"
          label=""
          activeLineWidth={0}
          disabledLineWidth={0}
          lineWidth={0}
          textColor="white"
        />
        <View style={style.sign_up_view}>
          <View style={style.sign_up_touch_view}>
            <TouchableOpacity
              style={style.sign_up_button}
              onPress={checkUserInput}>
              <Text style={style.sign_up_text}>Submit</Text>
            </TouchableOpacity>
          </View>
          <View style={style.verify_touch_view}>
            <TouchableOpacity
              style={style.manual_verify_link}
              onPress={() => navigation.navigate('VerifyManualSignUpScreen')}>
              <Text style={style.sign_up_text}>Manual Verify</Text>
            </TouchableOpacity>
          </View>
        </View>
        <SignNavComponent marginTop={20} sign_in={false} sign_up={true} />

        <View style={style.spinner}>
          <ActivityIndicator
            size="large"
            color="#fff"
            animating={signUpSpinner}
          />
        </View>

        <CladeAlert
          title="Sign Up"
          alert={signUpAlert}
          onConfirmPressed={() => {
            setSignUpAlert({status: false});
          }}
        />
      </>
    );
  }

  const SignUpX = () => {
    if (OS.ios()) {
      return <View style={style.main_view}>{SignUpObject()}</View>;
    } else {
      return <ScrollView style={style.main_view}>{SignUpObject()}</ScrollView>;
    }
  };

  return SignUpX();
}

const style = StyleSheet.create({
  main_view: {
    flex: 1,
    backgroundColor: color.blue,
    padding: 30,
    paddingTop: 10,
  },
  clade_image: {
    height: 90,
    width: 70,
  },
  welcome: {
    color: 'white',
    marginTop: 20,
    fontSize: 24,
  },
  sign_up_to: {
    color: 'gray',
    fontSize: 24,
  },
  text_input: {
    marginTop: 20,
    color: 'white',
    height: 50,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 16,
  },
  password_text_input: {
    color: 'white',
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 16,
  },
  sign_up_view: {
    marginTop: 30,
    flex: 1,
    flexDirection: 'row',
  },
  sign_up_text: {
    color: 'white',
    fontSize: OS.ios() ? 20 : 18,
  },
  spinner: {
    marginTop: 20,
  },
  sign_up_touch_view: {
    flex: 2,
    alignItems: 'flex-start',
  },
  verify_touch_view: {
    flex: 2,
    alignItems: 'flex-end',
  },
  sign_up_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
  manual_verify_link: {
    padding: 10,
  },
});

export default SignUpScreen;
