import React, {useState} from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import Pushwoosh from 'pushwoosh-react-native-plugin';
import config from '../../../config';

const color = globalStyles.color;

function Notify() {
  const [loading, setLoading] = useState(false);
  const [notificationsMessage, setNotificationsMessage] = useState('');

  const updateSettings = async (key) => {
    const settings_key = await AsyncStorage.getItem(config.settings_key);
    let settings = JSON.parse(settings_key);
    let updated_setting = Object.assign({}, settings, key);

    await AsyncStorage.setItem(
      config.settings_key,
      JSON.stringify(updated_setting),
    );
  };

  const setupNotify = () => {
    setLoading(true);
    updateSettings({push_notifications: true}).done();

    Pushwoosh.init({
      pw_appid: config.push.pw_appid,
      project_number: config.push.project_number,
    });

    Pushwoosh.register(
      (token) => {
        console.log('Registered for pushes: ' + token);
        Pushwoosh.getPushToken(function (token) {
          console.log('Push token: ' + token);
          setLoading(false);
          setNotificationsMessage('Notifications\nregistered.');
          // Geolocation tracking example
          //PushwooshGeozones.startLocationTracking();
          //PushwooshGeozones.stopLocationTracking();
        });
      },
      (error) => {
        setLoading(false);
        setNotificationsMessage(error);
        console.log('Failed to register: ' + error);
      },
    );

    Pushwoosh.getHwid((hwid) => {
      console.log('Pushwoosh hwid: ' + hwid);
    });
  };

  return (
    <View style={{marginTop: 20}}>
      <TouchableOpacity
        onPress={() => setupNotify()}
        style={{
          marginBottom: 30,
          borderRadius: 5,
          justifyContent: 'center',
          alignItems: 'center',
          width: 200,
          padding: 10,
          marginTop: 30,
          backgroundColor: color.stock_green,
        }}>
        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            color: 'white',
            fontSize: 20,
            fontWeight: 'bold',
          }}>
          OK
        </Text>
      </TouchableOpacity>
      <View style={{marginTop: 20}}>
        <ActivityIndicator size="large" color="pink" animating={loading} />
      </View>
      <View>
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18,
            justifyContent: 'center',
            textAlign: 'center',
          }}>
          {notificationsMessage}
        </Text>
      </View>
    </View>
  );
}

export default Notify;
