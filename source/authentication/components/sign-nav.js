import {StyleSheet, View} from 'react-native';
import SignInComponent from './sign-in-component';
import SignUpComponent from './sign-up-component';
import React from 'react';

const gray = 'gray';
const clade_pink = '#FD5D93';

function SignNavComponent(props) {
  function getSignInState(state) {
    if (state) {
      return clade_pink;
    }
    return gray;
  }

  function getSignUpState(state) {
    if (state) {
      return clade_pink;
    }
    return gray;
  }

  return (
    <View style={style(props).view_outer}>
      <View style={style(props).view_inner}>
        <View style={style(props).view_sign_component}>
          <SignInComponent
            activated={props.sign_in}
            color={getSignInState(props.sign_in)}
          />
        </View>
        <View>
          <SignUpComponent
            activated={props.sign_up}
            color={getSignUpState(props.sign_up)}
          />
        </View>
      </View>
    </View>
  );
}

const style = props =>
  StyleSheet.create({
    view_outer: {
      marginTop: props.marginTop,
      marginLeft: 10,
      marginRight: 10,
    },
    view_inner: {
      flexDirection: 'row',
    },
    view_sign_component: {
      flex: 2,
    },
  });

export default SignNavComponent;
