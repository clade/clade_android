import React from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

const icon_size = 32;

function SignInComponent(props) {
  const navigation = useNavigation();

  function navigateToSignIn(state) {
    if (!state) {
      navigation.goBack();
    }
  }

  return (
    <View style={style(props).view}>
      <Pressable
        style={style(props).pressable}
        onPress={() => navigateToSignIn(props.activated)}>
        <Icon name="log-in" size={icon_size} color={props.color} />
        <Text style={style(props).text}>Sign In</Text>
      </Pressable>
    </View>
  );
}

const style = props =>
  StyleSheet.create({
    view: {
      width: 60,
      alignItems: 'center',
    },
    pressable: {
      alignItems: 'center',
    },
    text: {
      marginTop: 5,
      color: props.color,
      fontSize: 14,
    },
  });

export default SignInComponent;
