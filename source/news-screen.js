import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  Image,
  View,
  Text,
  ImageBackground,
  FlatList,
  Linking,
} from 'react-native';
import HeaderH1 from './components/header-h1';
import axios from 'axios';
import Site from './helpers/site-helper';
import NewsHelper from './helpers/news-helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import CompanyNewsSearch from './search/components/company-news-search';
import OS from './helpers/os';

function NewsScreen() {
  const [firstHeadline, setFirstHeadline] = useState(false);
  const [headlines, setHeadlines] = useState([]);

  useEffect(() => {
    const fetchNewsHeadlines = async () => {
      try {
        const result = await axios(`${Site.getURL()}/news_headlines`);

        let headline_data = result.data;
        if (headline_data.length > 1) {
          setFirstHeadline(headline_data[0]);
          setHeadlines(headline_data.slice(1));
        }
      } catch (error) {
        console.log(error);
      }
    };

    fetchNewsHeadlines().done();
  }, []);

  const firstNewsImageURL = () => {
    if (firstHeadline) {
      return firstHeadline.article.url;
    }
  };

  const firstNewsHeadline = () => {
    if (firstHeadline) {
      return firstHeadline.article.source.name;
    }
  };

  const firstNewsTitle = () => {
    if (firstHeadline) {
      let sourceName = firstHeadline.article.source.name;
      return NewsHelper.removePublisher(firstHeadline.title, sourceName);
    }
  };

  const firstNewsImage = () => {
    if (firstHeadline) {
      return {uri: firstHeadline.article.url_image};
    }
  };

  const firstNewsTimeStamp = () => {
    if (firstHeadline) {
      return NewsHelper.getDate(firstHeadline.published_at.$date);
    }
  };

  const renderHeadline = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          Linking.openURL(item.article.url);
        }}>
        <View
          style={{
            marginBottom: 20,
            flex: 1,
            alignSelf: 'stretch',
            flexDirection: 'row',
          }}>
          <View style={{flex: 0.5, alignItems: 'flex-start'}}>
            <Image
              style={{marginTop: 10, height: 120, width: 120}}
              source={{
                uri: item.article.url_image,
              }}
            />
          </View>
          <View
            style={{
              marginLeft: 20,
              marginTop: 10,
              flex: 1,
              alignItems: 'flex-start',
            }}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              {item.title}
            </Text>

            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.5, alignItems: 'flex-start'}}>
                <Text style={{marginTop: 14, color: 'white', fontSize: 18}}>
                  {NewsHelper.getDate(item.published_at.$date)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, padding: 10, backgroundColor: '#1a1e34'}}>
      <CompanyNewsSearch />
      <View style={{height: OS.ios() ? 0 : 30}} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(firstNewsImageURL());
          }}>
          <ImageBackground
            style={{marginTop: 40, height: 280, width: '100%'}}
            source={firstNewsImage()}>
            <View
              style={{
                position: 'absolute',
                width: '100%',
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'white',
                  padding: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 5,
                  fontWeight: 'bold',
                  fontSize: 22,
                }}>
                {firstNewsHeadline()}
              </Text>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'white',
                  padding: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  fontWeight: 'bold',
                  fontSize: 16,
                }}>
                {firstNewsTitle()}
              </Text>
              <Text
                style={{
                  fontWeight: 'bold',
                  textAlign: 'left',
                  color: 'white',
                  padding: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 5,
                  fontSize: 12,
                }}>
                {firstNewsTimeStamp()}
              </Text>
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <View
          style={{marginTop: 5, marginBottom: 5, backgroundColor: '#eeeeee'}}>
          <Text
            style={{
              fontSize: 16,
              color: '#1a1e34',
              padding: 5,
              fontWeight: 'bold',
            }}>
            Important information
          </Text>
          <Text style={{fontSize: 14, color: '#1a1e34', padding: 5}}>
            Clade is not responsible for the content and accuracy of the
            articles and we may not share the views of the author.
          </Text>
        </View>
        <View style={{marginTop: 10}} />
        <HeaderH1 h1="Headlines" />
        <FlatList
          showsHorizontalScrollIndicator={false}
          legacyImplementation={false}
          data={headlines}
          renderItem={(item) => renderHeadline(item)}
          keyExtractor={(item) => item.symbol}
        />
      </ScrollView>
    </View>
  );
}

export default NewsScreen;
