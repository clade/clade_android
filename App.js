if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
}

import React, {useEffect, useState} from 'react';
import {Text, View, StatusBar, DeviceEventEmitter} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import config from './config';
import SignInScreen from './source/authentication/sign-in';
import DashBoardScreen from './source/dashboard-screen';
import WatchListScreen from './source/watchlist-screen';
import CompanySearchScreen from './source/company-search-screen';
import CompanyDetailsScreen from './source/search/company-details-screen';
import NewsScreen from './source/news-screen';
import SignUpScreen from './source/authentication/sign-up';
import SettingsScreen from './source/settings-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import VerifyManualSignUpScreen from './source/authentication/verify-manual-sign-up-screen';
import ForgottenPasswordScreen from './source/authentication/forgotten-password';
import PasswordResetScreen from './source/authentication/password-reset';
import VerifySignUpScreen from './source/authentication/verify-sign-up-screen';
import CompanySearchByIndustry from './source/search/company-search-by-industry';
import CompanyNewsScreen from './source/news/company-news-screen';
import OnBoarding from './source/authentication/on-boarding';
import Loading from './source/loading/loading';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import OS from './source/helpers/os';
import WelcomeScreen from './source/authentication/welcome-screen';

const App = () => {
  const [loading] = useState(true);
  const [onBoarded, setOnBoarded] = useState(null);

  useEffect(() => {
    const setup = async () => {
      try {
        const release_version = await AsyncStorage.getItem(
          config.release_version,
        );

        if (release_version == null) {
          await AsyncStorage.setItem(
            config.release_version,
            JSON.stringify('{release_version: 1.0.0}'),
          );
        }

        const auth = await AsyncStorage.getItem(config.auth_key);
        if (auth == null) {
          await AsyncStorage.setItem(config.auth_key, '{}');
        }

        const watchlist = await AsyncStorage.getItem(config.wish_list_key);
        if (watchlist === null) {
          await AsyncStorage.setItem(config.wish_list_key, '[]');
        }

        const recentlyViewed = await AsyncStorage.getItem(
          config.recently_viewed_key,
        );
        if (recentlyViewed === null) {
          await AsyncStorage.setItem(config.recently_viewed_key, '[]');
        }

        const portfolio = await AsyncStorage.getItem(config.portfolio_key);
        if (portfolio === null) {
          await AsyncStorage.setItem(config.portfolio_key, '[]');
        }

        const setting = await AsyncStorage.getItem(config.settings_key);
        if (setting === null) {
          await AsyncStorage.setItem(
            config.settings_key,
            JSON.stringify({
              push_notifications: false,
              email_notifications: false,
              portfolio_delete_confirmation: true,
              wish_list_delete_confirmation: true,
            }),
          );
        }

        const on_board = await AsyncStorage.getItem(config.on_boarding_key);
        if (on_board === null) {
          await AsyncStorage.setItem(
            config.on_boarding_key,
            JSON.stringify(false),
          );

          setOnBoarded(false);
        } else {
          setOnBoarded(JSON.parse(on_board));
        }

        DeviceEventEmitter.addListener('pushOpened', (e) => {
          console.log('pushOpened: ' + JSON.stringify(e));
          // alert(JSON.stringify(e));
        });

        DeviceEventEmitter.addListener('pushReceived', (e) => {
          console.log('pushReceived: ' + JSON.stringify(e));
          // shows a push is received. Implement passive reaction to a push, such as UI update or data download.
          // alert(JSON.stringify(e));
        });
      } catch (error) {
        // Error saving data
      }
      SplashScreen.hide();
    };

    setup().done();
  }, []);

  const screenOptions = {
    headerStyle: {
      shadowRadius: 0,
      backgroundColor: '#1a1e34',
      shadowOpacity: 0,
      shadowOffset: {
        height: 0,
      },
      borderBottomWidth: 0,
      elevation: 0,
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
    animationEnabled: false,
  };

  const LoadingScreensStack = createStackNavigator();
  function LoadingScreens() {
    return (
      <LoadingScreensStack.Screen
        name="Loading"
        component={Loading}
        options={{title: ''}}
      />
    );
  }

  const OnBoardingScreensStack = createStackNavigator();
  function OnBoardingScreens() {
    return (
      <OnBoardingScreensStack.Navigator screenOptions={screenOptions}>
        <OnBoardingScreensStack.Screen
          name="OnBoardingScreen"
          component={OnBoarding}
          options={{title: ''}}
        />
        <OnBoardingScreensStack.Screen
          name="SignInScreen"
          component={SignInScreen}
          options={{title: ''}}
        />
        <AuthenticationScreensStack.Screen
          name="SignUpScreen"
          component={SignUpScreen}
          options={{title: 'Sign Up'}}
        />
        <AuthenticationScreensStack.Screen
          name="VerifyManualSignUpScreen"
          component={VerifyManualSignUpScreen}
          options={{title: 'Manual Verify'}}
        />
        <AuthenticationScreensStack.Screen
          name="VerifySignUpScreen"
          component={VerifySignUpScreen}
          options={{title: 'Verify'}}
        />
        <AuthenticationScreensStack.Screen
          name="ForgottenPasswordScreen"
          component={ForgottenPasswordScreen}
          options={{title: 'Forgotten Password'}}
        />
        <AuthenticationScreensStack.Screen
          name="PasswordResetScreen"
          component={PasswordResetScreen}
          options={{title: 'Password Reset'}}
        />
        <AuthenticationScreensStack.Screen
          name="WelcomeScreen"
          component={WelcomeScreen}
          options={{title: 'Welcome'}}
        />
      </OnBoardingScreensStack.Navigator>
    );
  }

  const AuthenticationScreensStack = createStackNavigator();

  function AuthenticationsScreens() {
    return (
      <AuthenticationScreensStack.Navigator screenOptions={screenOptions}>
        <AuthenticationScreensStack.Screen
          name="SignInScreen"
          component={SignInScreen}
          options={{title: ''}}
        />
        <AuthenticationScreensStack.Screen
          name="SignUpScreen"
          component={SignUpScreen}
          options={{title: 'Sign Up'}}
        />
        <AuthenticationScreensStack.Screen
          name="VerifyManualSignUpScreen"
          component={VerifyManualSignUpScreen}
          options={{title: 'Manual Verify'}}
        />
        <AuthenticationScreensStack.Screen
          name="VerifySignUpScreen"
          component={VerifySignUpScreen}
          options={{title: 'Verify'}}
        />
        <AuthenticationScreensStack.Screen
          name="ForgottenPasswordScreen"
          component={ForgottenPasswordScreen}
          options={{title: 'Forgotten Password'}}
        />
        <AuthenticationScreensStack.Screen
          name="PasswordResetScreen"
          component={PasswordResetScreen}
          options={{title: 'Password Reset'}}
        />
        <AuthenticationScreensStack.Screen
          name="WelcomeScreen"
          component={WelcomeScreen}
          options={{title: 'Welcome'}}
        />
      </AuthenticationScreensStack.Navigator>
    );
  }

  const DashboardStack = createStackNavigator();

  function DashboardScreens() {
    return (
      <DashboardStack.Navigator screenOptions={screenOptions}>
        <DashboardStack.Screen
          name="DashBoardScreen"
          component={DashBoardScreen}
          options={{title: 'Dashboard'}}
        />
        <DashboardStack.Screen
          name="CompanyDetailsScreen"
          component={CompanyDetailsScreen}
          options={{title: ''}}
        />
      </DashboardStack.Navigator>
    );
  }

  const WatchListStack = createStackNavigator();

  function WatchListScreens() {
    return (
      <WatchListStack.Navigator screenOptions={screenOptions}>
        <WatchListStack.Screen
          name="WatchListScreen"
          component={WatchListScreen}
          options={{title: 'Watch List'}}
        />
        <WatchListStack.Screen
          name="CompanyDetailsScreen"
          component={CompanyDetailsScreen}
          options={{title: ''}}
        />
      </WatchListStack.Navigator>
    );
  }

  const CompanySearchSearchStack = createStackNavigator();

  function CompanySearchSearchScreens() {
    return (
      <CompanySearchSearchStack.Navigator screenOptions={screenOptions}>
        <CompanySearchSearchStack.Screen
          name="CompanySearchScreen"
          component={CompanySearchScreen}
          options={{title: 'Search'}}
        />
        <CompanySearchSearchStack.Screen
          name="CompanyDetailsScreen"
          component={CompanyDetailsScreen}
          options={{title: ''}}
        />
        <CompanySearchSearchStack.Screen
          name="CompanySearchByIndustry"
          component={CompanySearchByIndustry}
          options={{title: ''}}
        />
      </CompanySearchSearchStack.Navigator>
    );
  }

  const NewsStack = createStackNavigator();

  function NewsScreens() {
    return (
      <NewsStack.Navigator screenOptions={screenOptions}>
        <NewsStack.Screen
          name="NewsScreen"
          component={NewsScreen}
          options={{title: 'News'}}
        />
        <NewsStack.Screen
          name="CompanyNewsScreen"
          component={CompanyNewsScreen}
          options={{title: ''}}
        />
      </NewsStack.Navigator>
    );
  }

  const SettingsStack = createStackNavigator();

  function SettingsScreens() {
    return (
      <SettingsStack.Navigator screenOptions={screenOptions}>
        <SettingsStack.Screen
          name="SettingsScreen"
          component={SettingsScreen}
          options={{title: 'Settings'}}
        />
      </SettingsStack.Navigator>
    );
  }

  const tabBarIcon = (focused, iconName, name) => (
    <View style={{paddingTop: OS.ios() ? 10 : 8, alignItems: 'center'}}>
      <Icon
        name={iconName}
        size={OS.ios() ? 32 : 28}
        color={focused ? '#FD5D93' : '#1a1e34'}
      />
      <View>
        <Text style={{fontWeight: 'bold', color: focused ? '#1a1e34' : 'gray'}}>
          {name}
        </Text>
      </View>
    </View>
  );

  const Tab = createBottomTabNavigator();

  function Tabs() {
    return (
      <Tab.Navigator screenOptions={screenOptions}>
        <Tab.Screen
          name="DashboardScreens"
          component={DashboardScreens}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({focused}) =>
              tabBarIcon(focused, 'view-dashboard', 'Dash'),
          }}
        />
        <Tab.Screen
          name="WatchListScreens"
          component={WatchListScreens}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({focused}) => tabBarIcon(focused, 'eye', 'Watchlist'),
          }}
        />
        <Tab.Screen
          name="CompanySearchSearchScreens"
          component={CompanySearchSearchScreens}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({focused}) => tabBarIcon(focused, 'magnify', 'search'),
          }}
        />
        <Tab.Screen
          name="NewsScreens"
          component={NewsScreens}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({focused}) => tabBarIcon(focused, 'newspaper', 'News'),
          }}
        />
        <Tab.Screen
          name="SettingsScreens"
          component={SettingsScreens}
          options={{
            tabBarLabel: '',
            tabBarIcon: ({focused}) => tabBarIcon(focused, 'cog', 'Settings'),
          }}
        />
      </Tab.Navigator>
    );
  }

  const startUpScreen = () => {
    if (loading === null || onBoarded === null) {
      return LoadingScreens;
    }

    if (onBoarded === false) {
      return OnBoardingScreens;
    } else {
      return AuthenticationsScreens;
    }
  };

  const RootStack = createStackNavigator();
  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavigationContainer>
        <RootStack.Navigator>
          <RootStack.Screen
            name="OnBoardingAuthSwitch"
            component={startUpScreen()}
            options={{headerShown: false}}
          />
          <RootStack.Screen
            name="Tabs"
            component={Tabs}
            options={{headerShown: false}}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
